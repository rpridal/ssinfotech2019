class Address < ApplicationRecord
  def to_s
    street + " " + house_number + ", " + city
  end
end
